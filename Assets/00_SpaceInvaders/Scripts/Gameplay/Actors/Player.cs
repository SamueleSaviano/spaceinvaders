﻿using System.Collections;
using UnityEngine;

public class Player : Actor
{
    private Vector3 _direction;

    public static int Health;
    public static bool Resetting;

    public bool immune;

    private bool _inputReceived => InputManager.Left || InputManager.Right;
    private bool _limitReached
    {
        get
        {
            if (InputManager.Left)
            {
                return this.transform.position.x <= CameraManager.HorizontalMin + (this.size.x * 0.5f);
            }

            if (InputManager.Right)
            {
                return this.transform.position.x >= CameraManager.HorizontalMax - (this.size.x * 0.5f);
            }

            return false;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        this.type = ActorType.Player;
        GameManager.Player = this;
    }

    protected override void Start()
    {
        base.Start();

        Player.SetHealth(true);
    }

    public static void SetHealth(bool p_reset = false)
    {
        if (p_reset)
        {
            Player.Health = GameManager.Instance.gameData.maxHealth;
        }
        else
        {
            if (Player.Health > 0)
            {
                Player.Health--;
            }

            if (Player.Health == 0)
            {
                GameManager.Player.alive = false;
                GameManager.Instance.GameOver(GameResult.Lose);
            }
        }

        UIManager.Instance.UpdateHealth();
    }

    public void SetPosition()
    {
        this.transform.position = new Vector3(0, CameraManager.VerticalMin, this.transform.position.z);
    }

    public override void CustomUpdate()
    {
        if (!Player.Resetting)
        {
            base.CustomUpdate();
            base.CheckFire();
        }
    }

    protected override void Move()
    {
        base.Move();

        if (this._inputReceived)
        {
            if (InputManager.Left)
            {
                this._direction = Vector3.left;
            }

            if (InputManager.Right)
            {
                this._direction = Vector3.right;
            }

            if (!this._limitReached)
            {
                this.transform.position += this._direction * this.speed * Time.deltaTime;
            }
        }
    }

    public override void Toggle(bool p_activate)
    {
        base.Toggle(p_activate);
    }

    protected override void OnHit()
    {
        if (!this.immune)
        {
            base.OnHit();

            Player.SetHealth();

            this.explosionParticle.Play();
            this.ResetAfterHit();
        }
    }

    public void ResetAfterHit()
    {
        if (this.alive)
        {
            StartCoroutine(this.ResetAfterHitCO());
        }
    }

    private IEnumerator ResetAfterHitCO()
    {
        Player.Resetting = true;

        yield return new WaitUntil(() => !this.explosionParticle.isPlaying);

        this.SetPosition();
        this.Toggle(true);
        this.bullet.ResetBullet();

        Player.Resetting = false;
    }

    protected override void Fire()
    {
        base.Fire();
    }

    public override void ResetActor()
    {
        base.ResetActor();

        this.SetPosition();
        Player.SetHealth(true);
    }
}
