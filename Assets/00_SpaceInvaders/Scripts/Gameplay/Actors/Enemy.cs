﻿using UnityEngine;

public class Enemy : Actor
{
    [HideInInspector]
    public float xPos;    
    [HideInInspector]
    public int xIndex;

    private float _halfWidth => +this.size.x * 0.5f;

    public bool playerReached
    {
        get
        {
            return this.transform.position.y <= GameManager.Player.position.y + GameManager.Player.size.y;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        this.type = ActorType.Enemy;
    }

    protected override void Start()
    {
        base.Start();

        EnemyManager.AliveEnemies.Add(this);

        this.SetPosition();
        this.SetSprite(GameData.GetData<SpritesData>().GetEnemySprite());
    }

    private void SetPosition()
    {
        float _x = Mathf.Lerp(CameraManager.HorizontalMin + this._halfWidth, CameraManager.HorizontalMax - this._halfWidth,
                   Mathf.InverseLerp(0, EnemyManager.Instance.maxInRow - 1, this.xIndex));

        this.transform.localPosition = new Vector3(_x, 0, 0);

        this.xPos = _x;
    }

    public void SetSprite(Sprite p_sprite)
    {
        this._renderer.sprite = p_sprite;
    }

    public override void CustomUpdate()
    {
        base.CustomUpdate();
    }

    protected override void Fire()
    {
        base.Fire();
    }

    protected override void Move()
    {
        base.Move();
    }

    protected override void OnHit()
    {
        base.OnHit();

        this.alive = false;
        EnemyManager.AliveEnemies.Remove(this);

        GameManager.Instance.SetScore();
        EnemyManager.Instance.CheckAliveEnemies();
    }

    public override void Toggle(bool p_activate)
    {
        base.Toggle(p_activate);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }

    public override void ResetActor()
    {
        base.ResetActor();
    }
}
