﻿using UnityEngine;

public enum ActorType
{
    Player,
    Enemy
}

public class Actor : ExplodingObject
{
    public Vector3 position
    {
        get
        {
            return this.transform.position;
        }
    }

    [HideInInspector]
    public ActorType type;

    public Transform bulletSocket;
    public Bullet bullet;

    [Space]

    public float speed;

    [Space]

    [HideInInspector]
    public bool alive;
    [HideInInspector]
    public bool canFire;

    protected float _fireTimer;

    protected override void Awake()
    {
        base.Awake();
    }

    protected virtual void Start()
    {
        this.ResetActor();
    }

    public virtual void ResetActor()
    {
        this.alive = true;

        this.Toggle(true);
        this.bullet.SetUp(this);
    }

    public virtual void CustomUpdate()
    {
        this.Move();
    }

    public void CheckFire()
    {
        if (this.canFire)
        {
            this.Fire();
        }
    }

    protected virtual void Move()
    {

    }

    protected virtual void Fire()
    {
        if (this.canFire)
        {
            this.canFire = false;
            this._fireTimer = 0;

            this.bullet.Fire();
        }
    }


    public override void Toggle(bool p_activate)
    {
        base.Toggle(p_activate);
    }

    protected override void OnHit()
    {
        base.OnHit();
    }
}
