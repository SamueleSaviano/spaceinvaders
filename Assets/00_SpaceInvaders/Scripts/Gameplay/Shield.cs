﻿using UnityEngine;

public class Shield : MonoBehaviour
{
    public ShieldBlock[] blocks;
    private Vector3 _position;

    public int index;

    private float _size
    {
        get
        {
            return this.blocks[0].size.x * this.blocks.Length;
        }
    }

    public void SetPosition(int p_index)
    {
        this.index = p_index;

        this._position.z = 0;
        this._position.y = Mathf.Lerp(0, GameManager.Player.position.y, 0.5f);
        this._position.x = Mathf.Lerp(CameraManager.HorizontalMin + (this._size * 0.5f), CameraManager.HorizontalMax - (this._size * 0.5f),
                           Mathf.InverseLerp(0, GameManager.GameData.blocks - 1, this.index));

        this.transform.position = this._position;
    }

    public void ResetShield(int p_index)
    {
        this.SetPosition(p_index);

        foreach (ShieldBlock f_block in this.blocks)
        {
            f_block.Toggle(true);
        }
    }
}
