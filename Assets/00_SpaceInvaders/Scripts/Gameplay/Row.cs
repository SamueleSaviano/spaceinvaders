﻿using UnityEngine;

public class Row : MonoBehaviour
{
    private Vector3 _position;

    private float _initialDirection;
    private float _yPos;

    private int _hMovementStep;

    private int _maxSteps => GameData.GetData<EnemyData>().hMovementSteps;

    private bool _back;

    public void SetUp(int p_index, Vector2 p_enemySize)
    {
        this._initialDirection = p_index % 2 == 0 ? p_enemySize.x * 0.5f : -p_enemySize.x * 0.5f;

        this._yPos = CameraManager.VerticalMax - p_enemySize.y * 0.5f - (p_enemySize.y * p_index) - p_enemySize.y.Percentage(10f);
        this._position = new Vector3(0, this._yPos, 0);
        this.transform.position = this._position;

        this._hMovementStep = 1;
        this._back = false;
    }

    public void Move()
    {
        if(this._hMovementStep == 0 || this._hMovementStep == this._maxSteps)
        {
            this._back = !this._back;
        }

        this._hMovementStep += this._back ? -1 : 1;

        this._position.y = this._yPos;
        this._position.x = Mathf.Lerp(-this._initialDirection, this._initialDirection, 
                           Mathf.InverseLerp(0, 3, this._hMovementStep));

        this.transform.position = this._position;
    }

    public void GoDown(float p_offset)
    {
        this._yPos -= p_offset;
    }
}
