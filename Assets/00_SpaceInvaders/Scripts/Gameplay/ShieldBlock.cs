﻿using UnityEngine;

public class ShieldBlock : ExplodingObject
{
    protected override void Awake()
    {
        base.Awake();
    }

    public override void Toggle(bool p_activate)
    {
        base.Toggle(p_activate);
    }

    protected override void OnHit()
    {
        base.OnHit();
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
