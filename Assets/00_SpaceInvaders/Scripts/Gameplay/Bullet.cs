﻿using System.Collections;
using UnityEngine;

public class Bullet : ExplodingObject
{
    #region Private

    private Actor _actor;
    private Vector3 _direction;

    private bool _canMove;

    #endregion

    #region Public

    [Space]

    public float speed;

    #endregion

    protected override void Awake()
    {
        base.Awake();
    }

    public void SetUp(Actor p_actor)
    {
        this._actor = p_actor;
        this._direction = p_actor.type == ActorType.Player ? Vector3.up : Vector3.down;

        this.ResetBullet();
    }

    public void Fire()
    {
        this.transform.SetParent(GameManager.Instance.bulletsPool);

        this.Toggle(true);
        this._canMove = true;
    }

    private void Update()
    {
        if (this._canMove)
        {
            this.transform.position += this._direction * this.speed * Time.deltaTime;

            if (!this._renderer.isVisible && Vector3.Distance(this.transform.position, this._actor.transform.position) > this._actor.size.y)
            {
                this.ResetBullet();
            }
        }
    }

    public void ResetBullet()
    {
        this._canMove = false;
        this.Toggle(false);

        this.transform.SetParent(this._actor.bulletSocket);
        this.transform.localPosition = Vector3.zero;

        this._actor.canFire = true;
    }

    public override void Toggle(bool p_activate)
    {
        base.Toggle(p_activate);
    }

    protected override void OnHit()
    {
        this._canMove = false;
        base.OnHit();

        StartCoroutine(this.OnHitCO());
    }

    private IEnumerator OnHitCO()
    {
        yield return new WaitUntil(() => !this.explosionParticle.isPlaying);

        this.ResetBullet();
    }
}
