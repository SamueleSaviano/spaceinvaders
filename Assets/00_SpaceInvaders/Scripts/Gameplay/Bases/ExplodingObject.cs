﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class ExplodingObject : MonoBehaviour
{
    protected SpriteRenderer _renderer;
    protected BoxCollider2D _collider;

    protected bool _hitted;

    public Vector3 size
    {
        get
        {
            if (this._collider == null)
            {
                this.Awake();
            }

            return this._collider.size * this.transform.lossyScale;
        }
    }

    public ParticleSystem explosionParticle;

    protected virtual void Awake()
    {
        this._renderer = this.GetComponent<SpriteRenderer>();
        this._collider = this.GetComponent<BoxCollider2D>();
    }

    public virtual void Toggle(bool p_activate)
    {
        this._collider.enabled = p_activate;
        this._renderer.enabled = p_activate;
    }

    protected virtual void OnHit()
    {
        this.Toggle(false);
        this.explosionParticle.Play();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (!this._hitted)
        {
            this._hitted = true;

            this.OnHit();
            StartCoroutine(this.ResetHit());
        }
    }

    protected IEnumerator ResetHit()
    {
        yield return new WaitForSeconds(0.1f);
        this._hitted = false;
    }
}
