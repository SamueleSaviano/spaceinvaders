﻿using UnityEngine;

[CreateAssetMenu(fileName = "Sprites Data", menuName = "Data/Sprites Data")]
public class SpritesData : ScriptableObject
{
    public Sprite[] enemySprites;

    public Sprite GetEnemySprite()
    {
        return this.enemySprites.Random();
    }
}
