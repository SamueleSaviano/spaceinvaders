﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Data", menuName = "Data/Game Data")]
public class GameData : ScriptableObject
{
    public ScriptableObject[] data;

    [Space]

    public int maxHealth;

    [Space]

    public float borderXOffset;
    public float borderYOffset;

    [Space]

    public int blocks;

    [Space]

    public string victoryString;
    public string defeatString;

    public static T GetData<T>() where T : ScriptableObject
    {
        return (T)GameManager.GameData.data.ToList().Find(_x => _x.GetType() == typeof(T));
    }
}
