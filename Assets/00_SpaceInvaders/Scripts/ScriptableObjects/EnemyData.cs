﻿using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Data", menuName = "Data/Enemy Data")]
public class EnemyData : ScriptableObject
{
    public int scorePerEnemy;

    [Space]

    public int enemyRows;

    [Space]

    public float minFireTime;
    public float maxFireTime;

    [Space]

    public int hMovementSteps;
    public float hMovementFreq;
    public float vMovementFreq;
    public float vMovementMinFreq;

    [Space]

    public float frequencyModifier;
    public float movementModifier;
}
