﻿using UnityEngine;

[CreateAssetMenu(fileName = "Prefabs Data",menuName = "Data/Prefabs Data")]
public class PrefabsData : ScriptableObject
{
    public GameObject enemy;
    public GameObject shield;
}
