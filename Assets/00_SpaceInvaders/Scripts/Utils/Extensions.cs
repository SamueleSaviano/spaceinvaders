﻿using System.Collections.Generic;
public static class Extensions
{
    public static T Random<T>(this T[] p_array)
    {
        return p_array[UnityEngine.Random.Range(0, p_array.Length)];
    }

    public static T Random<T>(this List<T> p_list)
    {
        return p_list[UnityEngine.Random.Range(0, p_list.Count)];
    }

    public static float Percentage(this float p_value, float p_percentage)
    {
        return p_value / 100f * p_percentage;
    }

    public static bool IsInRange(this float p_value, float p_min, float p_max, bool p_includeEdges = true)
    {
        if (p_includeEdges)
        {
            return p_value >= p_min && p_value <= p_max;
        }

        return p_value > p_min && p_value < p_max;
    }
}
