﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    #region Private

    private EnemyData _enemyData => GameData.GetData<EnemyData>();

    private List<Row> _rows = new List<Row>();
    private List<Enemy> _enemies = new List<Enemy>();
    private List<Enemy> _closestEnemies = new List<Enemy>();

    private GameObject _rowClone;
    private Row _row;

    private GameObject _enemyPrefab;
    private GameObject _enemyClone;
    private Enemy _enemy;

    private Vector3 _enemySize;

    private int _rowsNum;

    private float _fireFrequency;
    private float _fireTimer;
    private float _hMovementTimer;
    private float _vMovementTimer;
    private float _vMovementFrequency;

    #endregion

    #region Public

    public static List<Enemy> AliveEnemies = new List<Enemy>();
    public static EnemyManager Instance;

    public Transform enemyPool;

    [HideInInspector]
    public int maxInRow;

    [Space]

    public bool fire = true;
    public bool move = true;    

    #endregion

    private void Awake()
    {
        EnemyManager.Instance = this;
    }

    private void Start()
    {
        this._enemyPrefab = GameData.GetData<PrefabsData>().enemy;        
        this._enemySize = this._enemyPrefab.GetComponent<Enemy>().size;

        this._rowsNum = this._enemyData.enemyRows;
        this._vMovementFrequency = this._enemyData.vMovementFreq;

        this.CreateGrid();
    }

    private void Update()
    {
        if (GameManager.CheckGameState(GameState.Playing))
        {
            for (int i = 0; i < EnemyManager.AliveEnemies.Count; i++)
            {
                if (EnemyManager.AliveEnemies[i].alive)
                {
                    EnemyManager.AliveEnemies[i].CustomUpdate();
                }
            }

            this.Movement();
            this.Fire();
        }
    }

    private void Movement()
    {
        if (!this.move)
        {
            return;
        }

        this._hMovementTimer += Time.deltaTime;

        if(this._hMovementTimer >= this._enemyData.hMovementFreq)
        {
            this._hMovementTimer = 0;

            for (int i = 0; i < this._rows.Count; i++)
            {
                this._rows[i].Move();
            }
        }

        this._vMovementTimer += Time.deltaTime;

        if(this._vMovementTimer >= this._vMovementFrequency)
        {
            this._vMovementTimer = 0;

            if (this._vMovementFrequency - this._enemyData.vMovementMinFreq > 0)
            {
                this._vMovementFrequency -= this._vMovementFrequency.Percentage(this._enemyData.frequencyModifier);
            }

            for (int i = 0; i < this._rows.Count; i++)
            {
                this._rows[i].GoDown(this._enemySize.y.Percentage(this._enemyData.movementModifier));
            }

            this.CheckPayerReached();
        }
    }

    private void CheckPayerReached()
    {
        foreach (Enemy f_enemy in EnemyManager.AliveEnemies)
        {
            if (f_enemy.playerReached)
            {
                GameManager.Instance.GameOver(GameResult.Lose);
                break;
            }
        }
    }

    private void Fire()
    {
        if (!this.fire)
        {
            return;
        }

        if (GameManager.CheckGameState(GameState.Playing) && !Player.Resetting)
        {
            this._fireTimer += Time.deltaTime;

            if(this._fireTimer >= this._fireFrequency)
            {
                this._fireTimer = 0;
                this.UpdateFireFrequency();

                if (EnemyManager.AliveEnemies.Count > 0)
                {
                    this.GetClosestEnemies();

                    if (this._closestEnemies.Count > 0)
                    {
                        this._closestEnemies.Random().CheckFire();
                    }
                    else
                    {
                        EnemyManager.AliveEnemies.Random().CheckFire();
                    }
                }
            }
        }
    }

    public void UpdateFireFrequency()
    {
        this._fireFrequency = Random.Range(this._enemyData.minFireTime, this._enemyData.maxFireTime);
    }

    public void CheckAliveEnemies()
    {
        if(EnemyManager.AliveEnemies.Count == 0)
        {
            GameManager.Instance.GameOver(GameResult.Won);
        }
    }

    private void GetClosestEnemies()
    {
        Enemy _enemy;
        float _playerPos = GameManager.Player.transform.position.x;

        this._closestEnemies.Clear();

        for (int i = 0; i < EnemyManager.AliveEnemies.Count; i++)
        {
            _enemy = EnemyManager.AliveEnemies[i];

            if(_enemy.xPos.IsInRange(_playerPos - GameManager.Player.size.x, _playerPos + GameManager.Player.size.x))
            {
                this._closestEnemies.Add(_enemy);
            }
        }
    }

    private void CreateGrid()
    {
        this.maxInRow = Mathf.FloorToInt(CameraManager.Width / this._enemySize.x);

        for (int i = 0; i < this._rowsNum; i++)
        {
            this.CreateRow(i);
        }
    }

    private void CreateRow(int p_index)
    {
        this._rowClone = new GameObject($"Row ({p_index})");
        this._rowClone.transform.SetParent(this.enemyPool);

        this._row = this._rowClone.AddComponent<Row>();
        this._rows.Add(this._row);

        for (int i = 0; i < this.maxInRow; i++)
        {
            this.CreateEnemy(this._rowClone.transform, i, p_index);
        }

        this._row.SetUp(p_index, this._enemySize);
    }

    private void CreateEnemy(Transform _row, int p_index, int p_row)
    {
        this._enemyClone = Instantiate(this._enemyPrefab, Vector3.zero, Quaternion.identity, _row);
        this._enemy = this._enemyClone.GetComponent<Enemy>();

        this._enemy.xIndex = p_index;

        this._enemies.Add(this._enemy);
    }

    public void ResetEnemies()
    {
        EnemyManager.AliveEnemies.Clear();

        foreach (Enemy f_enemy in this._enemies)
        {
            f_enemy.ResetActor();
            EnemyManager.AliveEnemies.Add(f_enemy);
        }

        for (int i = 0; i < this._rows.Count; i++)
        {
            this._rows[i].SetUp(i, this._enemySize);
        }

        this.UpdateFireFrequency();
    }
}
