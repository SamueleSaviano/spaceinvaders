﻿using System.Collections.Generic;
using UnityEngine;

public enum ScreenType
{
    Menu,
    HUD,
    GameOver
}

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    public static MenuScreen CurrentScreen;

    public List<MenuScreen> menuScreens = new List<MenuScreen>();

    private HUD _hud;

    public MenuScreen GetScreen(ScreenType p_type)
    {
        if (this.menuScreens.Count > 0)
        {
            return this.menuScreens.Find(_x => _x.type == p_type);
        }

        return null;
    }

    public T GetScreenScript<T>() where T : MenuScreen
    {
        return (T)this.menuScreens.Find(_x => _x.GetComponent<T>() != null);
    }

    private void Awake()
    {
        UIManager.Instance = this;
    }

    private void Start()
    {
        this._hud = this.GetScreenScript<HUD>();

        this.ChangeScreen(ScreenType.Menu);
    }

    public void AddScreen(MenuScreen p_screen)
    {
        if(this.GetScreen(p_screen.type) == null)
        {
            this.menuScreens.Add(p_screen);
            p_screen.ExitScreen();
        }
    }

    public void ChangeScreen(ScreenType p_newScreen)
    {
        if(UIManager.CurrentScreen != null)
        {
            UIManager.CurrentScreen.ExitScreen();
        }

        UIManager.CurrentScreen = this.GetScreen(p_newScreen);
        UIManager.CurrentScreen.EnterScreen();
    }

    public void UpdateHealth()
    {
        this._hud.SetHealth();
    }

    public void UpdateScore()
    {
        this._hud.SetScore();
    }
}
