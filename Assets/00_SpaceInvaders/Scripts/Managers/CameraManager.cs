﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static Camera GameCamera;

    public static float HalfHeight;
    public static float HalfWidth;

    private static float XOffset;
    private static float YOffset;

    public Transform topLimit;
    public Transform bottomLimit;
    public Transform leftLimit;
    public Transform rightLimit;

    public bool showLimits;

    public static float HorizontalMin 
    { 
        get 
        { 
            return -CameraManager.HalfWidth + CameraManager.XOffset; 
        } 
    }
    public static float HorizontalMax
    {
        get
        {
            return CameraManager.HalfWidth - CameraManager.XOffset;
        }
    }

    public static float VerticalMin
    {
        get
        {
            return -CameraManager.HalfHeight + CameraManager.YOffset;
        }
    }
    public static float VerticalMax
    {
        get
        {
            return CameraManager.HalfHeight - CameraManager.YOffset;
        }
    }

    public static float Width { get { return CameraManager.HorizontalMax * 2f; } }
    public static float Height { get { return CameraManager.VerticalMax * 2f; } }

    private void Awake()
    {
        CameraManager.GameCamera = Camera.main;
        CameraManager.GameCamera.transform.position = new Vector3(0, 0, -10f);

        CameraManager.HalfHeight = CameraManager.GameCamera.orthographicSize;
        CameraManager.HalfWidth = CameraManager.GameCamera.aspect * CameraManager.HalfHeight;

        CameraManager.XOffset = CameraManager.HalfWidth.Percentage(GameManager.GameData.borderXOffset);
        CameraManager.YOffset = CameraManager.HalfHeight.Percentage(GameManager.GameData.borderYOffset);
    }

    private void Start()
    {
        this.topLimit.gameObject.SetActive(this.showLimits);
        this.bottomLimit.gameObject.SetActive(this.showLimits);
        this.leftLimit.gameObject.SetActive(this.showLimits);
        this.rightLimit.gameObject.SetActive(this.showLimits);

        if (showLimits)
        {
            this.topLimit.position = new Vector3(0, CameraManager.VerticalMax, 0);
            this.bottomLimit.position = new Vector3(0, CameraManager.VerticalMin, 0);
            this.leftLimit.position = new Vector3(CameraManager.HorizontalMin, 0, 0);
            this.rightLimit.position = new Vector3(CameraManager.HorizontalMax, 0, 0);
        }
    }
}
