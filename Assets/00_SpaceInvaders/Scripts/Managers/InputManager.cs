﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    public static bool Left;
    public static bool Right;

    public KeyCode leftInput;
    public KeyCode rightInput;

    private void Awake()
    {
        InputManager.Instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (GameManager.CheckGameState(GameState.Start))
            {
                GameManager.SetGameState(GameState.Playing);
            }
        }

        InputManager.Left = Input.GetKey(this.leftInput) && !InputManager.Right;
        InputManager.Right = Input.GetKey(this.rightInput) && !InputManager.Left;
    }
}
