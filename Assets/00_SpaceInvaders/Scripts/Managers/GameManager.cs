﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Start,
    Playing,
    GameOver
}

public enum GameResult
{
    Won,
    Lose
}

public class GameManager : MonoBehaviour
{
    private GameObject _blockPrefab;
    private GameObject _shieldClone;
    private Shield _shield;

    public static GameManager Instance;
    public static Player Player;
    public static GameData GameData;

    public static GameState GameState;
    public static GameResult GameResult;

    public static int MaxHealth => GameManager.GameData.maxHealth;
    public static int Score;

    public GameData gameData;

    [Space]

    public Transform bulletsPool;
    public Transform shieldsPool;

    [Space]

    public List<Shield> shields = new List<Shield>();

    public static bool CheckGameState(GameState p_state)
    {
        return GameManager.GameState == p_state;
    }

    private void Awake()
    {
        GameManager.Instance = this;
        GameManager.GameData = this.gameData;

        this._blockPrefab = GameData.GetData<PrefabsData>().shield;
    }

    private void Start()
    {
        GameManager.SetGameState(GameState.Start);

        GameManager.Player.SetPosition();
        this.CreateBlocks();
    }

    public static void SetGameState(GameState p_newState)
    {
        GameManager.GameState = p_newState;
    }

    private void Update()
    {
        if (GameManager.CheckGameState(GameState.Playing))
        {
            if (GameManager.Player.alive)
            {
                GameManager.Player.CustomUpdate();
            }
        }
    }

    public void ResetGame()
    {
        GameManager.Player.ResetActor();
        EnemyManager.Instance.ResetEnemies();

        for (int i = 0; i < this.shields.Count; i++)
        {
            this.shields[i].ResetShield(i);
        }
    }

    public void StartGame()
    {
        this.SetScore(true);

        StartCoroutine(this.OnChangeState(() =>
        {
            EnemyManager.Instance.UpdateFireFrequency();
            GameManager.SetGameState(GameState.Playing);
        }));
    }

    public void GameOver(GameResult p_result)
    {
        GameManager.GameResult = p_result;

        StartCoroutine(this.OnChangeState(() =>
        {
            GameManager.SetGameState(GameState.GameOver);
            UIManager.Instance.ChangeScreen(ScreenType.GameOver);
        }));
    }

    private IEnumerator OnChangeState(Action p_action)
    {
        yield return new WaitForSeconds(0.5f);
        p_action?.Invoke();
    }

    public void SetScore(bool p_reset = false)
    {
        if (p_reset)
        {
            GameManager.Score = 0;
        }
        else
        {
            GameManager.Score += GameData.GetData<EnemyData>().scorePerEnemy;
        }

        UIManager.Instance.UpdateScore();
    }

    private void CreateBlocks()
    {
        for (int i = 0; i < GameManager.GameData.blocks; i++)
        {
            this.CreateShield(i);
        }
    }

    private void CreateShield(int p_index)
    {
        this._shieldClone = Instantiate(this._blockPrefab, Vector3.zero, Quaternion.identity, this.shieldsPool);
        this._shield = this._shieldClone.GetComponent<Shield>();

        this.shields.Add(this._shield);

        this._shield.SetPosition(p_index);
    }
}
