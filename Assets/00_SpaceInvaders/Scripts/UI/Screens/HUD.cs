﻿using UnityEngine.UI;

public class HUD : MenuScreen
{
    public Text score;
    public Image[] hearts;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();

        GameManager.Instance.StartGame();
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void SetHealth()
    {
        for (int i = 0; i < this.hearts.Length; i++)
        {
            this.hearts[i].enabled = i <= Player.Health - 1;
        }
    }

    public void SetScore()
    {
        this.score.text = GameManager.Score.ToString();
    }
}
