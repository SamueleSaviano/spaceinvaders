﻿
public class MainScreen : MenuScreen
{
    protected override void Awake()
    {
        base.Awake();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void PlayButton()
    {
        UIManager.Instance.ChangeScreen(ScreenType.HUD);
    }
}
