﻿using UnityEngine.UI;

public class GameOverScreen : MenuScreen
{
    public Text result;
    public Text score;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();

        switch (GameManager.GameResult)
        {
            case GameResult.Won:

                this.result.text = GameManager.GameData.victoryString;
                break;
            case GameResult.Lose:

                this.result.text = GameManager.GameData.defeatString;
                break;
        }

        this.score.text = $"Score: {GameManager.Score}";
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void HomeButton()
    {
        GameManager.Instance.ResetGame();
        UIManager.Instance.ChangeScreen(ScreenType.Menu);
    }
}
