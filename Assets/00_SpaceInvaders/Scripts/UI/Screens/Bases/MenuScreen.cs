﻿using UnityEngine;

public class MenuScreen : MonoBehaviour
{
    public ScreenType type;

    protected virtual void Awake()
    {
        UIManager.Instance.AddScreen(this);        
    }

    public virtual void EnterScreen()
    {
        this.gameObject.SetActive(true);
    }

    public virtual void ExitScreen()
    {
        this.gameObject.SetActive(false);
    }
}
